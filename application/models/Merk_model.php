<?php

class Merk_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function getAll($id_merk = null){
		$this->db->select('*');
		$this->db->from('tb_merk');

		if ($id_merk != null) {
			$this->db->where('id_merk', $id_merk);
		}

		return $this->db->get();
	}

	public function insert($data){
		$this->db->insert('tb_merk', $data);
		return TRUE;
	}

	public function delete($table, $par, $var){
		$this->db->where($par, $var);
		$this->db->delete($table);
	}

	public function update($id, $data){
		$this->db->where('id_merk', $id);
		$this->db->update('tb_merk', $data);
	}
}
