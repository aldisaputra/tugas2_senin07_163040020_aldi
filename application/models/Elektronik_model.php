<?php

class Elektronik_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function getData($id_elektronik = null){
		$this->db->select('id, nama_produk, tb_merk.nama_merk as merk, tb_kategori.nama_kategori as kategori, warna, gambar, harga, deskripsi, stok');
		$this->db->from('tb_elektronik');
		$this->db->join('tb_kategori', 'tb_elektronik.kategori = tb_kategori.id_kategori');
		$this->db->join('tb_merk', 'tb_elektronik.merk = tb_merk.id_merk');

		if ($id_elektronik == null) {
			$this->db->order_by('id', 'asc');
		}else {
			$this->db->where('id', $id_elektronik);
		}

		return $this->db->get();
	}

    //fungsi insert ke database
    public function insert($data){
       $this->db->insert('tb_elektronik', $data);
       return TRUE;
    }

    public function delete($table, $par, $var){
    	$this->db->where($par, $var);
    	$this->db->delete($table);
	}

	public function update($id, $data) {
		$this->db->where('id', $id);
		$this->db->update("tb_elektronik", $data);
	}
}
