<div class="main mt-5 pt-4">
    <div class="content container wow fadeIn">
        <div class="cartoption">		
            <div class="cartpage">
                <?php if ($this->cart->total_items()) { ?>
                    <h2>Your Cart</h2>
                    <table class="table table-striped" style="margin-top: 25px;">
                        <tr>
                            <th width="5%">Sr.</th>
                            <th width="30%">Product Name</th>
                            <th width="10%">Image</th>
                            <th width="15%">Price</th>
                            <th width="20%">Quantity</th>
                            <th width="15%">Total Price</th>
                            <th width="5%">Remove</th>
                        </tr>
                        <?php
                        $i = 0;
                        foreach ($elektronik as $cart_items) {
                            $i++;
                            ?>
                            
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $cart_items['name'] ?></td>
                                <td><img src="<?php echo base_url('assets/img/resize/' . $cart_items['gambar']) ?>" alt=""/></td>
                                <td>Rp. <?php echo $this->cart->format_number($cart_items['price']) ?></td>
                                <td>
                                    <form action="<?php echo base_url('web/update_cart'); ?>" method="post">
                                        <input type="number" name="qty" value="<?php echo $cart_items['qty'] ?>"/>
                                        <input type="hidden" name="rowid" value="<?php echo $cart_items['rowid'] ?>"/>
                                        <input type="submit" name="submit" value="Update" class="btn-warning"/>
                                    </form>
                                </td>
                                <td>Rp. <?php echo $this->cart->format_number($cart_items['subtotal']) ?></td>
                                <td>
                                    <form action="<?php echo base_url('web/remove_cart'); ?>" method="post">
                                        <input type="hidden" name="rowid" value="<?php echo $cart_items['rowid'] ?>"/>
                                        <input type="submit" name="submit" class="btn-danger" value="X"/>
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>


                    </table>
                    <table style="float:right;text-align:left;" width="40%">
                        <tr>
                            <th>Grand Total : </th>
                            <td>Rp. <?php echo $this->cart->format_number($this->cart->total()); ?> </td>
                        </tr>
                    </table>
                    <?php
                } else {
                    echo "<h1>Your Cart Empty</h1>";
                }
                ?>
            </div>
            <style>
            .shopleft {
                float: left;
            }
            .shopleft a,.shopright a{outline:none;}
            </style>
            <div class="shopping">
                <div class="shopleft">
                    <a href="<?= base_url('web/') ?>" class="nav-link waves-effect">
                        <span class="btn blue-gradient btn-lg"><i class="fa fa-shopping-bag"></i> Continue Shopping </span></a>
                    </a>
                </div>
                <div class="shopright">
                    <?php
                    if (empty($this->cart->total_items())) {
                            ?>
                            <a href="<?= base_url('web/') ?>" onclick="return alert('Your cart is empty')" class="nav-link waves-effect">
                                <span class="btn peach-gradient btn-lg"><i class="fa fa-credit-card"></i>&nbsp;&nbsp;&nbsp;&nbsp;Checkout&nbsp;&nbsp;&nbsp;&nbsp;</span></a>
                            </a>
                            <script type="text/javascript">'; 
                                function info() {
                                    alert("Your Cart is Empty");
                                    window.location= "index.php";
                                }
                            </script>
                            <?php
                    } else if (isset($this->session->userdata['logged_in']['id_user'])) {
                            ?>
                                <a href="<?= base_url('web/customer_shipping') ?>" class="nav-link waves-effect">
                                    <span class="btn peach-gradient btn-lg"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;&nbsp;&nbsp;Checkout&nbsp;&nbsp;&nbsp;&nbsp;</span></a>
                                </a>
                            <?php
                    } else {
                            ?>
                            <a href="<?= base_url('auth/') ?>" class="nav-link waves-effect">
                                <span class="btn peach-gradient btn-lg"><i class="fa fa-credit-card"></i>&nbsp;&nbsp;&nbsp;&nbsp;Checkout&nbsp;&nbsp;&nbsp;&nbsp;</span></a>
                            </a>
                            <?php
                    }
                    ?>
                </div>
            </div>
        </div>  	
        <div class="clear"></div>
    </div>
</div>
