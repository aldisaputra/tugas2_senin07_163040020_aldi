  <!--Main layout-->
  <main class="mt-5 pt-4">
    <div class="container dark-grey-text mt-5">

      <!--Grid row-->
      <div class="row wow fadeIn">

        <!--Grid column-->
        <div class="col-md-6 mb-4">

          <img src="<?= base_url('assets/img/') ?><?= $elektronik->gambar ?>" class="img-fluid" alt="">

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-6 mb-4">

          <!--Content-->
          <div class="p-4">

            <p class="lead font-weight-bold"><?= $elektronik->nama_produk ?></p>

            <div class="mb-3">
                <span class="badge purple mr-1"><?= $elektronik->kategori ?></span>
            </div>

            <p class="lead">
              <span><?="Rp. ". number_format($elektronik->harga,0,',','.'); ?></span>
            </p>

            <p class="lead font-weight-bold">Description</p>

            <p><?= $elektronik->deskripsi ?></p>

            <div class="mb-3">
                <span>Warna : </span><span class="badge black mr-1"><?= $elektronik->warna ?></span>
            </div>
            <div class="mb-3">
                <span>Stok : </span><span class="mr-1"><?= $elektronik->stok ?></span>
            </div>

            <form class="d-flex justify-content-left" action="<?php echo base_url('web/save_cart');?>" method="post">
              <input type="hidden" class="buyfield" name="id" value="<?= $elektronik->id ?>">
              <input type="number" name="qty" value="1" class="buyfield quantity form-control" style="width: 100px">
              <button class="btn btn-primary btn-md my-0 p buysubmit" name="submit">Add to cart
                <i class="fa fa-shopping-cart ml-1"></i>
              </button>
            </form>

          </div>
          <!--Content-->

        </div>
        <!--Grid column-->

      </div>
      <!--Grid row-->

      <hr>

      

      </div>
      <!--Grid row-->

      <!--Grid row-->

      <div class="row wow fadeIn">
      <?php foreach ($rekomen as $key): ?>

        <!--Grid column-->
        <div class="col-lg-4 col-md-6 mb-4">
          <a href="<?= base_url();?>web/detail/<?= $key->id ?>" >
            <img src="<?= base_url('assets/img/') ?><?= $key->gambar ?>" class="img-fluid" style="height:250px;">
          </a>
          
        </div>
        <!--Grid column-->
        <?php endforeach; ?>
      </div>
      <!--Grid row-->

     
    </div>
  </main>
  <!--Main layout-->