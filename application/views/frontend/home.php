  <!--Main layout-->
  <main>
    <div class="container">

      <!--Navbar-->
      <nav class="navbar navbar-expand-lg navbar-dark mdb-color lighten-3 mt-3 mb-5">

        <!-- Navbar brand -->
        <span class="navbar-brand">Categories:</span>

        <!-- Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
          aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Collapsible content -->
        <div class="collapse navbar-collapse" id="basicExampleNav">

          <!-- Links -->
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="<?= base_url();?>web">All
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url();?>web/product/2">Televisi</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url();?>web/product/3">Home Theater</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url();?>web/product/5">GPS</a>
            </li>

          </ul>
          <!-- Links -->
        </div>
        <!-- Collapsible content -->

      </nav>
      <!--/.Navbar-->

      <!--Section: Products v.3-->
      <section class="text-center mb-4">

        <!--Grid row-->
        <div class="row wow fadeIn">

          <?php if (!empty($elektronik)) : ?>
          <?php foreach ($elektronik as $key): ?>

          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">

              <!--Card image-->
              <div class="view overlay">
                <img src="<?= base_url('assets/img/') ?><?= $key->gambar ?>" class="card-img-top" alt="">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>
              <!--Card image-->

              <!--Card content-->
              <div class="card-body text-center">
                <!--Category & Title-->
                <a href="<?= base_url();?>web/detail/<?= $key->id ?>" class="grey-text">
                  <h5><?= $key->kategori; ?></h5>
                </a>
                <h5>
                  <strong>
                    <a href="<?= base_url();?>web/detail/<?= $key->id ?>" class="dark-grey-text"><?= $key->nama_produk; ?>
                    </a>
                  </strong>
                </h5>

                <h4 class="font-weight-bold blue-text">
                  <strong><?="Rp. ". number_format($key->harga,0,',','.'); ?></strong>
                </h4>

              </div>
              <!--Card content-->

            </div>
            <!--Card-->

          </div>
          <!--Grid column-->

          <?php endforeach; ?>
          <?php else : ?>
          <tr>
            <td colspan="10"><h3>Data yang dicari tidak ditemukan</h3></td>
          </tr>
        <?php endif;?>
        </div>
        <!--Grid row-->

      </section>
      <!--Section: Products v.3-->
    </div>
  </main>
  <!--Main layout-->
