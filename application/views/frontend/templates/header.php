<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Elekstornics</title>
  <link rel="icon" href="<?= base_url();?>/assets/img/icon.png">
  <!-- Font Awesome -->
  <link href="<?= base_url();?>/assets/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- Bootstrap core CSS -->
  <link href="<?= base_url();?>/assets/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?= base_url();?>/assets/css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="<?= base_url();?>/assets/css/style.min.css" rel="stylesheet">
  <style type="text/css">
    html,
    body,
    header,
    .carousel {
      height: 60vh;
    }

    @media (max-width: 740px) {
      html,
      body,
      header,
      .carousel {
        height: 100vh;
      }
    }

    @media (min-width: 800px) and (max-width: 850px) {
      html,
      body,
      header,
      .carousel {
        height: 100vh;
      }
    }
  </style>
</head>

<body>

  <!-- Navbar -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
    <div class="container">

      <!-- Brand -->
      <a class="navbar-brand waves-effect" href="<?= base_url('web/') ?>">
        <strong class="blue-text">ELEKSTORNICS</strong>
      </a>

      <!-- Collapse -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Links -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <!-- Left -->
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link waves-effect" href="<?= base_url('web/') ?>">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
        </ul>

        <!-- Right -->
        <ul class="navbar-nav nav-flex-icons">
          <li class="nav-item">
            <a href="<?= base_url('web/cart') ?>" class="nav-link waves-effect">
              <span class="badge red z-depth-1 mr-1"> <?php echo $this->cart->total_items();?> </span>
              <i class="fa fa-shopping-cart"></i>
              <span class="clearfix d-none d-sm-inline-block"> Cart </span></a>
            </a>
          </li>
          <li class="nav-item">
          <?php
              if (isset($this->session->userdata['logged_in'])) {
              $nama_depan = ($this->session->userdata['logged_in']['nama_depan']);
          ?>
        <div class="dropdown">
          <a href="<?= base_url('auth/logout') ?>" class="nav-link border border-light rounded waves-effect dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-user mr-2"></i><?= $nama_depan ?>
          </a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a href="<?= base_url('auth/logout') ?>" class="nav-link border border-light rounded waves-effect dropdown-item">
              <i class="fa fa-sign-out mr-2"></i>Sign Out
            </a>
          </div>
        </div>
          <?php
            } else {
          ?>
            <a href="<?= base_url('auth') ?>" class="nav-link border border-light rounded waves-effect">
              <i class="fa fa-sign-in mr-2"></i>Sign In
            </a>
            <?php
            }
            ?>
            
          </li>
        </ul>

      </div>

    </div>
  </nav>
  <!-- Navbar -->