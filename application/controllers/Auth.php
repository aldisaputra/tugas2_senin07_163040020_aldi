<?php


Class Auth extends CI_Controller {

	public function __construct() {
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');
		// Load form validation library
		$this->load->library('form_validation');
		// Load session library
		$this->load->library('session');
		// Load database
		$this->load->model('Login_model');
	}

	public function index() {
		// Show login page
		$this->load->view('frontend/login');
	}
	

	// Show registration page
	public function user_registration_show() {
		$this->load->view('frontend/registrasi');
	}

	// Validate and store registration data in database
	public function registration() {

		// Check validation for user input in SignUp form
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
		$this->form_validation->set_rules('nama_depan', 'Nama Depan', 'trim|required|xss_clean');
		$this->form_validation->set_rules('nama_belakang', 'Nama Belakang', 'trim|required|xss_clean');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('frontend/registrasi');
		} else {
			$data = array(
				'nama_depan' => $this->input->post('nama_depan'),
				'nama_belakang' => $this->input->post('nama_belakang'),
				'username' => $this->input->post('username'),
				'email' => $this->input->post('email'),
				'password' => md5($this->input->post('password'))
			);
			$result = $this->Login_model->registration_insert($data);
			if ($result == TRUE) {
				$data['message_display'] = 'Registration Successfully !';
				$this->load->view('frontend/login', $data);
			} else {
				$data['message_display'] = 'Username already exist!';
				$this->load->view('frontend/registrasi', $data);
			}
		}
	}

		// Check for user login process
	public function user_login_process() {

		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			if(isset($this->session->userdata['logged_in'])){
				redirect('web/');
			}else{
				$this->load->view('frontend/login');
			}
		} 
		else {
			$data = array(
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password'))
			);
			$result = $this->Login_model->login($data);
			if ($result == TRUE) {

				$username = $this->input->post('username');
				$result = $this->Login_model->read_user_information($username);
				if ($result != false) {
					$session_data = array(
						'id_user' => $result[0]->id_user,
						'username' => $result[0]->username,
						'email' => $result[0]->email,
						'nama_depan' => $result[0]->nama_depan
					);
					// Add user data in session
					$this->session->set_userdata('logged_in', $session_data);
					redirect('web/');
				}
			} 
			else {
				$data = array(
					'error_message' => 'Invalid Username or Password'
				);
				$this->load->view('frontend/login', $data);
			}
		}
	}

	public function admin() {

		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			if(isset($this->session->userdata['admin_logged_in'])){
				redirect('elektronik/');
			}else{
				$this->load->view('frontend/login_admin');
			}
		} 
		else {
			$data = array(
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password'))
			);
			$result = $this->Login_model->login_admin($data);
			if ($result == TRUE) {

				$username = $this->input->post('username');
				$result = $this->Login_model->read_admin_information($username);
				if ($result != false) {
					$session_data = array(
						'id_admin' => $result[0]->id_admin,
						'username' => $result[0]->username,
						'foto' => $result[0]->foto,
						'nama' => $result[0]->nama
					);
					// Add user data in session
					$this->session->set_userdata('admin_logged_in', $session_data);
					redirect('elektronik/');
				}
			} 
			else {
				$data = array(
					'error_message' => 'Invalid Username or Password'
				);
				$this->load->view('frontend/login_admin', $data);
			}
		}
	}


	// Logout from admin page
	public function logout() {

		// Removing session data
		$sess_array = array(
			'username' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data['message_display'] = 'Successfully Logout';
		redirect('web/');
	}

	public function logout_admin() {

		// Removing session data
		$sess_array = array(
			'username' => ''
		);
		$this->session->unset_userdata('admin_logged_in', $sess_array);
		$data['message_display'] = 'Successfully Logout';
		redirect('auth/admin');
	}

}

?>