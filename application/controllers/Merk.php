<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index(){
		$data['merk'] = $this->Merk_model->getAll(null)->result();

		$this->load->view('backend/templates/header');
		$this->load->view('backend/templates/sidebar');
		$this->load->view('backend/merk_view', $data);
		$this->load->view('backend/templates/footer');
	}

	public function insert(){
		$data = array(
			'id_merk' => $this->input->post('id'),
			'nama_merk' => $this->input->post('merk')
		);

		$status = $this->Merk_model->insert($data);
		if ($status > 0) {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-info\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Success : </b> Merk berhasil ditambahkan
				</div>
			</div>");
			redirect('merk/');
		}else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Merk gagal ditambahkan
				</div>
			</div>");
			redirect('merk/');
		}
	}

	public function delete($id_merk){
		$res = $this->Merk_model->delete('tb_merk', 'id_merk', $id_merk);
		if ($res >= 0) {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-info\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>X</span>
					</button>
					<b>Success : </b> Data Berhasil dihapus
				</div>
			</div>");
			redirect('merk/');
		}else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Data gagal dihapus
				</div>
			</div>");
			redirect('merk/');
		}
	}

	public function update(){
		$id_merk = $this->input->post('id_merk');
		$data = array(
			'nama_merk' => $this->input->post('merk')
		);
		$res = $this->Merk_model->update($id_merk, $data);
		if ($res >= 0) {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
				<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
			</button>
					<div class=\"alert-icon\">
						<i class=\"fa fa-info-circle\"></i>
					</div>
					
					<b>Success : </b> Data Berhasil diubah
				</div>
			</div>");
			redirect('Merk/');
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Data gagal diubah
				</div>
			</div>");
			redirect('Merk/');
		}
	}
}
