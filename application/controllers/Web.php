<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('cart');
    }

    public function index()
    {
        $data['elektronik'] = $this->Web_model->getAllProduct(null)->result();
        $this->load->view('frontend/templates/header');
        $this->load->view('frontend/templates/slider');
        $this->load->view('frontend/home', $data);
        $this->load->view('frontend/templates/footer');
    }

    public function detail($id) {
        $data['elektronik'] = $this->Web_model->getAllProduct($id)->row();
        $data['rekomen'] = $this->Web_model->getRekomen()->result();
        $this->load->view('frontend/templates/header');
        $this->load->view('frontend/detail', $data);
        $this->load->view('frontend/templates/footer');
    }

    public function cart() {
        $data = array();
        $data['elektronik'] = $this->cart->contents();
        $this->load->view('frontend/templates/header');
        $this->load->view('frontend/cart_view', $data);
        $this->load->view('frontend/templates/footer');
    }

    public function product($kategori) {
        $data['elektronik'] = $this->Web_model->getProduct($kategori)->result();
        $this->load->view('frontend/templates/header');
        $this->load->view('frontend/templates/slider');
        $this->load->view('frontend/home', $data);
        $this->load->view('frontend/templates/footer');
    }

    public function category_post($id) {
        $data = array();
        $data['get_all_product'] = $this->web_model->get_all_product_by_cat($id);
        $this->load->view('web/inc/header');
        $this->load->view('web/pages/product', $data);
        $this->load->view('web/inc/footer');
    }

    public function save_cart() {
        $id = $this->input->post('id');
        $arr = $this->Web_model->getAllProduct($id)->row();
        $data['id'] = $arr->id;
        $data['name'] = $arr->nama_produk;
        $data['price'] = $arr->harga;
        $data['qty'] = $this->input->post('qty');
        $data['gambar'] = $arr->gambar;

        $this->cart->insert($data);
        redirect('web/cart');
    }

    public function update_cart() {
        $data = array();
        $data['qty'] = $this->input->post('qty');
        $data['rowid'] = $this->input->post('rowid');

        $this->cart->update($data);
        redirect('web/cart');
    }

    public function remove_cart() {

        $data = $this->input->post('rowid');
        $this->cart->remove($data);
        redirect('web/cart');
    }

    public function customer_shipping() {
        $data = array();
        $this->load->view('frontend/templates/header');
        $this->load->view('frontend/checkout');
        $this->load->view('frontend/templates/footer');
    }

    public function save_customer() {
        $data = array();
        $data['nama_depan'] = $this->input->post('nama_depan');
        $data['nama_belakang'] = $this->input->post('nama_belakang');
        $data['email'] = $this->input->post('email');
        $data['no_hp'] = $this->input->post('no_hp');
        $data['alamat'] = $this->input->post('alamat');
        
        $this->form_validation->set_rules('nama_depan', 'Nama Depan', 'trim|required');
        $this->form_validation->set_rules('nama_belakang', 'Nama Belakang', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('no_hp', 'No HP', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');

        if ($this->form_validation->run() == true) {
            $result = $this->Web_model->save_customer($data);
            $this->session->set_userdata('id_customer', $result);
            if ($result) {
                redirect('web/save_order');
            } else {
                $this->session->set_flashdata('message', '<div class=\"alert alert-danger\">
                <div class=\"container-fluid\">
                        <div class=\"alert-icon\">
                            <i class=\"fa fa-exclamation\"></i>
                        </div>
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
                            <span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
                        </button>
                        <b>Error : </b> Pembelian Gagal
                    </div>
                </div>');
                redirect('web/customer_shipping');
            }
        } else {
            $this->session->set_flashdata('message', validation_errors());
            redirect('web/customer_shipping');
        }
    }

    public function save_order() {
        $odata = array();
        $odata['id_user'] = $this->session->userdata['logged_in']['id_user'];
        $odata['id_customer'] = $this->session->userdata('id_customer');
        $odata['order_total'] = $this->cart->total();

        $order_id = $this->Web_model->save_order($odata);
        if ($order_id) {
            $oddata = array();

            $myoddata = $this->cart->contents();
    
            foreach ($myoddata as $oddatas) {
    
    
                $oddata['id_order'] = $order_id;
                $oddata['id_produk'] = $oddatas['id'];
                $oddata['stok_terjual'] = $oddatas['qty'];
                $this->Web_model->save_order_details($oddata);
            }
    
            $this->cart->destroy();
    
            redirect('web');;
        } else {
            $this->session->set_flashdata('message', '<div class=\"alert alert-danger\">
            <div class=\"container-fluid\">
                    <div class=\"alert-icon\">
                        <i class=\"fa fa-exclamation\"></i>
                    </div>
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
                        <span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
                    </button>
                    <b>Error : </b> Pembelian Gagal
                </div>
            </div>');
            redirect('web/');
        }
    }
}