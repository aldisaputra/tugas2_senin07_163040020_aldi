<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Elektronik extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['elektronik'] = $this->Elektronik_model->getData(null)->result();
		$data['kategori'] = $this->Kategori_model->getKategori(null)->result();
		$data['merk'] = $this->Merk_model->getAll(null)->result();

		$this->load->view('backend/templates/header');
		$this->load->view('backend/templates/sidebar');
		$this->load->view('backend/elektronik_view', $data);
		$this->load->view('backend/templates/footer');
	}

	public function insert()
	{
		
		$this->load->library('upload');

		$file = "img_" . time();
		$con['upload_path'] = './assets/img/';
		$con['allowed_types'] = 'gif|bmp|jpg|png|jpeg';
		$con['max_size'] = 3000;
		$con['max_width'] = 1928;
		$con['max_height'] = 1024;
		$con['file_name'] = $file;
		
		$this->upload->initialize($con);

		$name = $_FILES['foto']['name'];
		if (isset($name)) {
			if ($this->upload->do_upload('foto')) {
				$img = $this->upload->data();
				$data = array(
					'gambar' => $img['file_name'],
					'nama_produk' => $this->input->post('nama_produk'),
					'merk' => $this->input->post('merk'),
					'kategori' => $this->input->post('kategori'),
					'warna' => $this->input->post('warna'),
					'harga' => $this->input->post('harga'),
					'stok' => $this->input->post('stok'),
					'deskripsi' => $this->input->post('deskripsi')
					);

				$this->Elektronik_model->insert($data);

				$con2['image_library'] = 'gd2';
				$con2['source_image'] = $this->upload->upload_path . $this->upload->file_name;
				$con2['new_image'] = './assets/img/resize/';
				$con2['maintain_ratio'] = TRUE;
				$con2['width'] = 100;
				$con2['height'] = 100;
				$this->load->library('image_lib', $con2);

				if (!$this->image_lib->resize()) {
					$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
				}
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-info\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Success : </b> Produk berhasil ditambahkan
				</div>
			</div>");
				redirect('Elektronik/');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Produk gagal ditambahkan
				</div>
			</div>");
				redirect('Elektronik/');
			}
		}

	}

	public function update()
	{
		
		$this->load->library('upload');

		$data['nama_produk'] = $this->input->post('nama_produk');
		$data['merk'] = $this->input->post('merk');
		$data['kategori'] = $this->input->post('kategori');
		$data['warna'] = $this->input->post('warna');
		$data['stok'] = $this->input->post('stok');
		$data['harga'] = $this->input->post('harga');
		$data['deskripsi'] = $this->input->post('deskripsi');
		$id = $this->input->post('id');
		$status = $this->Elektronik_model->update($id, $data);

		$file = "img_" . time();
		$con['upload_path'] = './assets/img/';
		$con['allowed_types'] = 'gif|bmp|jpg|png|jpeg';
		$con['max_size'] = 3000;
		$con['max_width'] = 1928;
		$con['max_height'] = 1024;
		$con['file_name'] = $file;
		
		$this->upload->initialize($con);


		if ($_FILES['foto']['name']) {
			if ($this->upload->do_upload('foto')) {
				$img = $this->upload->data();
				$data['gambar'] = $img['file_name'];
				$this->Elektronik_model->update($id, $data);

				$con2['image_library'] = 'gd2';
				$con2['source_image'] = $this->upload->upload_path . $this->upload->file_name;
				$con2['new_image'] = './assets/img/resize/';
				$con2['maintain_ratio'] = TRUE;
				$con2['width'] = 100;
				$con2['height'] = 100;
				$this->load->library('image_lib', $con2);

				if (!$this->image_lib->resize()) {
					$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
				}
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
				<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
			</button>
					<div class=\"alert-icon\">
						<i class=\"fa fa-info-circle\"></i>
					</div>
					
					<b>Success : </b> Data Berhasil diubah
				</div>
			</div>");
				redirect('Elektronik/');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Data gagal diubah
				</div>
			</div>");
				redirect('Elektronik/');
			}
		}
		if ($status >= 0) {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
				<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
			</button>
					<div class=\"alert-icon\">
						<i class=\"fa fa-info-circle\"></i>
					</div>
					
					<b>Success : </b> Data Berhasil diubah
				</div>
			</div>");
			redirect('Elektronik/');
		}else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Data gagal diubah
				</div>
			</div>");
			redirect('Elektronik/');
		}
	}

	public function delete($id)
	{
		$status = $this->Elektronik_model->delete('tb_elektronik', 'id', $id);
		if ($status >= 0) {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-info\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>X</span>
					</button>
					<b>Success : </b> Data Berhasil dihapus
				</div>
			</div>");
			redirect("Elektronik/");
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Data gagal dihapus
				</div>
			</div>");
			redirect("Elektronik/");
		}
	}


}
